﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetitorManagerOnline.Migrations
{
    public partial class buildNewDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Coaches",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    Nationality = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coaches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    CoachId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teams_Coaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Competitors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Nationality = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false),
                    Sport = table.Column<string>(nullable: true),
                    TeamId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Competitors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Competitors_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Coaches",
                columns: new[] { "Id", "Age", "FirstName", "LastName", "Nationality" },
                values: new object[,]
                {
                    { 1, 44, "Coach1", "Coach1", "Norway" },
                    { 2, 44, "Coach2", "Coach2", "England" },
                    { 3, 44, "Coach3", "Coach3", "Sudan" },
                    { 4, 44, "Coach4", "Coach4", "North Korea" },
                    { 5, 44, "Coach5", "Coach5", "Iran" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CoachId", "Name" },
                values: new object[,]
                {
                    { 1, 1, "Team1" },
                    { 2, 2, "Team2" },
                    { 3, 3, "Team3" },
                    { 4, 4, "Team4" },
                    { 5, 5, "Team5" }
                });

            migrationBuilder.InsertData(
                table: "Competitors",
                columns: new[] { "Id", "Age", "FirstName", "Height", "LastName", "Nationality", "Sport", "TeamId" },
                values: new object[,]
                {
                    { 1, 23, "test1", 176, "test1", "testLand1", "Ice-Hockey", 1 },
                    { 2, 24, "test2", 177, "test2", "testLand2", "Badmington", 2 },
                    { 3, 25, "test3", 178, "test3", "testLand3", "Volleyball", 3 },
                    { 4, 26, "test4", 179, "test4", "testLand4", "Basketball", 4 },
                    { 5, 27, "test5", 180, "test5", "testLand5", "Football", 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_TeamId",
                table: "Competitors",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Teams_CoachId",
                table: "Teams",
                column: "CoachId",
                unique: true,
                filter: "[CoachId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Competitors");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "Coaches");
        }
    }
}
