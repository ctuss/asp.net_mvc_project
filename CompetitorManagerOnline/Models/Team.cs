﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitorManagerOnline.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CoachId { get; set; }
        public Coach Coach { get; set; }
        public ICollection<Competitor> Competitors { get; set; }
    }
}
