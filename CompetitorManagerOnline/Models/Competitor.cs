﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitorManagerOnline.Models
{
    public class Competitor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nationality { get; set; }
        public int Age { get; set; }
        public int Height { get; set; }
        public string Sport { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
    }
}
