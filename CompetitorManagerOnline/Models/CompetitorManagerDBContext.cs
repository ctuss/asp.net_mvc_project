﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitorManagerOnline.Models
{
    public class CompetitorManagerDBContext : DbContext
    {
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Coach> Coaches { get; set; }

        public CompetitorManagerDBContext(DbContextOptions<CompetitorManagerDBContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Competitor>().HasData(new Competitor { Id = 1, FirstName = "test1", LastName = "test1", Nationality = "testLand1", Age = 23, Height = 176, Sport = "Ice-Hockey", TeamId = 1 });
            modelBuilder.Entity<Competitor>().HasData(new Competitor { Id = 2, FirstName = "test2", LastName = "test2", Nationality = "testLand2", Age = 24, Height = 177, Sport = "Badmington", TeamId = 2 });
            modelBuilder.Entity<Competitor>().HasData(new Competitor { Id = 3, FirstName = "test3", LastName = "test3", Nationality = "testLand3", Age = 25, Height = 178, Sport = "Volleyball", TeamId = 3 });
            modelBuilder.Entity<Competitor>().HasData(new Competitor { Id = 4, FirstName = "test4", LastName = "test4", Nationality = "testLand4", Age = 26, Height = 179, Sport = "Basketball", TeamId = 4 });
            modelBuilder.Entity<Competitor>().HasData(new Competitor { Id = 5, FirstName = "test5", LastName = "test5", Nationality = "testLand5", Age = 27, Height = 180, Sport = "Football", TeamId = 5 });

            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 1, FirstName = "Coach1", LastName = "Coach1", Age = 44, Nationality = "Norway" });
            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 2, FirstName = "Coach2", LastName = "Coach2", Age = 44, Nationality = "England" });
            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 3, FirstName = "Coach3", LastName = "Coach3", Age = 44, Nationality = "Sudan" });
            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 4, FirstName = "Coach4", LastName = "Coach4", Age = 44, Nationality = "North Korea" });
            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 5, FirstName = "Coach5", LastName = "Coach5", Age = 44, Nationality = "Iran" });

            modelBuilder.Entity<Team>().HasData(new Team { Id = 1, Name = "Team1", CoachId = 1 });
            modelBuilder.Entity<Team>().HasData(new Team { Id = 2, Name = "Team2", CoachId = 2 });
            modelBuilder.Entity<Team>().HasData(new Team { Id = 3, Name = "Team3", CoachId = 3 });
            modelBuilder.Entity<Team>().HasData(new Team { Id = 4, Name = "Team4", CoachId = 4 });
            modelBuilder.Entity<Team>().HasData(new Team { Id = 5, Name = "Team5", CoachId = 5 });
        }
    }
}
