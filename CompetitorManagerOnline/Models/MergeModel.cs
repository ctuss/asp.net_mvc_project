﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitorManagerOnline.Models
{
    public class MergeModel
    {
        public Competitor Competitor { get; set; }
        public Team Team { get; set; }
    }
}
