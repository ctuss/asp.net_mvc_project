﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CompetitorManagerOnline.Models;
using Microsoft.EntityFrameworkCore;

namespace CompetitorManagerOnline.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private CompetitorManagerDBContext _dbContext;

        public List<Competitor> allCompetitors;
        public List<MergeModel> mergeModelList = new List<MergeModel>();

        public HomeController(ILogger<HomeController> logger, CompetitorManagerDBContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public IActionResult Index()
        {
            GetAllCompetitors();
            ViewData["CompetitorList"] = mergeModelList;
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public async Task<IActionResult> DisplayInfo(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var competitor = await _dbContext.Competitors
                .FirstOrDefaultAsync(m => m.Id == id);
            var team = await _dbContext.Teams.FirstOrDefaultAsync(t => t.Id == competitor.TeamId);
            if (competitor == null)
            {
                return NotFound();
            }
            var mergemodel = new MergeModel { Competitor = competitor, Team = team };
            return View(mergemodel);
        }

        [HttpPost]
        public IActionResult RegisterCompetitor(string firstName, string lastName, string nationality, int age, int height, string sport)
        {
            Competitor competitor = new Competitor
            {
                FirstName = firstName,
                LastName = lastName,
                Nationality = nationality,
                Age = age,
                Height = height,
                Sport = sport
            };

            _dbContext.Competitors.Add(competitor);
            _dbContext.SaveChanges();

            GetAllCompetitors();
            ViewData["CompetitorList"] = mergeModelList;
            return View("Index");
        }

        public void GetAllCompetitors()
        {
            allCompetitors = null;
            allCompetitors = _dbContext.Competitors.Where(comp => comp.Id > 0).ToList();

            foreach (Competitor comp in allCompetitors)
            {
                var team = _dbContext.Teams.Find(comp.TeamId);
                if (team != null)
                {
                    mergeModelList.Add(new MergeModel { Competitor = comp, Team = team });
                }
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
