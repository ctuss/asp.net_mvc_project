# CompetitorManagerOnline
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
> CompetitorManagerOnline Assignment
## Table of Contents
- [Run](#run)
- [Maintainers](#maintainers)
- [Components](#components)
- [Contributing](#contributing)
- [License](#license)
## Run
```
Visual Studio Run
```
## Components
Application to register competitors, teams and coaches. Disclaimer, coaches and teams are deprecated for the time being, extensive developers team are on the case to fix issues.
## Maintainers
[Christopher Toussi (@ctuss)](https://gitlab.com/ctuss)
## Contributing
PRs accepted.
Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.
## License
Experis © 2020 Noroff Accelerate AS